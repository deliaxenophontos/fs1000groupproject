const form = document.querySelector('form');
const username = document.getElementById('username');
const password = document.getElementById('password');
const para = document.querySelector('p.validationDisplay');
const signin = document.querySelector('.signIn');

signin.addEventListener('click', function(e) {

    if (username.value === '' || password.value === '') {
        e.preventDefault();
        para.style.display = "block"
        para.textContent = 'You need to fill in both username and password!';
    }

    else if (password.value.length < 8) {
        e.preventDefault();
        para.style.display = "block"
        para.textContent = 'The password must be at least 8 characters in length!';
    }

    else if (username.value === 'admin' && password.value === 'Admin123') {
        e.preventDefault();
        para.style.display = "block"
        window.location.href="admin.html";
    }
    
    else {
        e.preventDefault();
        para.style.display = "block"
        para.textContent = 'Authentication Failed!'
    }

})